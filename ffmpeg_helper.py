#!/usr/bin/env python3

import ffmpeg
import json
import sys
import argparse
import pathlib


def pretty_print_json(data):
    print(json.dumps(data, sort_keys=True, indent=4))


def scan_file(file):
    print("Analyzing file '" + file.as_posix() + "'.")
    try:
        pretty_print_json(ffmpeg.probe(file.as_posix()))
    except ffmpeg.Error as e:
        print("ffprobe error:")
        print("-----stdout-----")
        print(e.stdout.decode("utf-8"))
        print("-----stderr-----")
        print(e.stderr.decode("utf-8"))


def scan_dir(directory):
    print("Iterating through directory '" + directory.as_posix() + "'.")
    for file in directory.iterdir():
        scan_file(file)


def main(a):
    file = pathlib.Path(a.file).resolve()

    if not file.exists():
        print("File '" + file.as_posix() + "' doesn't exist.")
        sys.exit(1)

    if file.is_dir():
        scan_dir(file)
    elif file.is_file():
        scan_file(file)


if __name__ == "__main__":
    try:
        args = argparse.ArgumentParser(description="ffmpeg helper for managing your video library.")

        args.add_argument("-v", "--verbose", dest="verbose", action="store_true", help="Verbose output")
        args.add_argument("-s", "--scan", dest="scan", action="store_true", help="Only scan file or directory")

        args.add_argument("file", help="File or directory to analyze")
        main(args.parse_args())
    except KeyboardInterrupt:
        print("Caught keyboard interrupt (Ctrl+C)")
        sys.exit(1)
